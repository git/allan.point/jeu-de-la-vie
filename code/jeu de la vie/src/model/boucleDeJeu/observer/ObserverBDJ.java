package model.boucleDeJeu.observer;

/**
 * Permet d'obbserver une boucle de jeu
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface ObserverBDJ {
	/**
	 * Réaction en cas de notification
	 */
	void update();
}
