package model.cellule;

/**
 * Tout les etats possible pour la manipulation de cellules
 */
public enum CellState {
	/**
	 * La cellule est vivante
	 */
	LIVE,

	/**
	 * La cellule est morte ou meur
	 */
	DIE,

	/**
	 * La cellule née
	 */
	BIRTH
}
