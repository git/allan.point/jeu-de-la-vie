package model.cellule.observer;

import model.cellule.Cellule;

/**
 * Permet d'obbserver une boucle de jeu
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface ObserverCellule {
	/**
	 * Réaction en cas de notification
	 */
	void update(Cellule cellule);
}
