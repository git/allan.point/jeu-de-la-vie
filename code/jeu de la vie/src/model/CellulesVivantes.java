package model;

import model.cellule.Cellule;
import model.cellule.observer.ObserverCellule;
import model.cellule.Position;

import java.util.HashMap;

/**
 * Représentation des cellules vivantes sur le plateau. Elle se met à jours automatiquement.
 * @author Yohann Breuil
 * @author Allan Point
 */
public class CellulesVivantes implements ObserverCellule {

	/**
	 * Dictionaire contenant toutes les cellules vivantes
	 */
	private HashMap<Position, Cellule> cellVivantes;

	public CellulesVivantes(){
		this(new HashMap<>());
	}

	private CellulesVivantes(HashMap<Position, Cellule> cellVivantes){
		this.cellVivantes = cellVivantes;
	}

	/**
	 * Récuperer une cellule vivante
	 * @param x Absisse de la cellule
	 * @param y Ordonée de la cellule
	 * @return La cellule (x; y) si elle est vivante. Sinon null
	 */
	public Cellule getAt(int x, int y){
		Position p = new Position(x, y);
		return cellVivantes.get(p);
	}

	/**
	 * Ajoute une paire clef:valeur (Postion:Cellule) dans le dictionaire contenant les cellules vivantes
	 * @param cell Cellule à ajouter
	 * @see Position
	 * @see Cellule
	 */
	private void addPeer(Cellule cell){	cellVivantes.put(cell.getPosition(), cell);	}

	/**
	 * Retir une paire clef:valeur (Postion:Cellule) du dictionaire contenant les cellules vivantes
	 * @param cellule Cellule à retirer
	 * @see Position
	 * @see Cellule
	 */
	private void rmPeer(Cellule cellule){
		cellVivantes.remove(cellule.getPosition());
	}

	/**
	 * Comportement lors ce que le cellule notifit l'objet CellulesVivantes.
	 * Ici on ajoute ou retire la cellule du dictionaire qui contient les cellules vivante en fonction de la cellule qui à notifiée.
	 * @param cellule Cellule qui à notifiée
	 */
	@Override
	public void update(Cellule cellule) {
		if(cellule.isAlive()){
			addPeer(cellule);
		} else {
			rmPeer(cellule);
		}
	}

	/**
	 * Cloner l'objet
	 * @return Le meme objet CellulesVivantes avec une référence diférente
	 */
	public CellulesVivantes clone(){
		return new CellulesVivantes(new HashMap<>(cellVivantes));
	}

	/**
	 * Nétoie le dictionaire contenant les cellules vivantes
	 */
	public void reset(){
		cellVivantes = new HashMap<>();
	}
}
