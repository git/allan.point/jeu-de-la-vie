package model.actualiseur;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Actualiseur de tours qui incrémente le numéro de génération 1 par 1
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ActualiseurTourUnParUn implements ActualiseurTour{

	private IntegerProperty cptTour = new SimpleIntegerProperty();
		public int getcptTour(){return cptTour.get();}
		private void setCptTour(int valeur){cptTour.set(valeur);}
		public IntegerProperty cptTourProperty(){return cptTour;}

	public ActualiseurTourUnParUn(){
		resetTour();
	}

	/**
	 * Incrémneter le numéro de génération de 1
	 */
	@Override
	public void changerTour() {
		setCptTour(getcptTour()+1);
	}

	/**
	 * Réinitialiser le numéro de génération à 0
	 */
	@Override
	public void resetTour(){
		cptTour.set(0);
	}
}
