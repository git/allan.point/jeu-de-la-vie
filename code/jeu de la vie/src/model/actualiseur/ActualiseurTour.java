package model.actualiseur;

/**
 * Comportement à adopter quand on veut actualiser des tours
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface ActualiseurTour {
	/**
	 * Changer le numéro de génération
	 */
	void changerTour();

	/**
	 * Réinitialiser le numéro de génération
	 */
	void resetTour();
}
