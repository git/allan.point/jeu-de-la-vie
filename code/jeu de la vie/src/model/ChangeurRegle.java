package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.actualiseur.ActualiseurCellule;
import model.actualiseur.ActualiseurEtatCellule;
import model.arbitre.ArbitreConwayStyle;
import model.arbitre.ArbitreKiller;
import model.plateau.Plateau;

/**
 * Permet de gerer le changement de règles
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ChangeurRegle {
	private ObjectProperty<Regle> regleEnCours = new SimpleObjectProperty<>();
		public Regle getRegleEnCours() { return regleEnCours.get();	}
		public void setRegleEnCours(Regle regleEnCours) { this.regleEnCours.set(regleEnCours); }
		public ObjectProperty<Regle> regleEnCoursProperty() { return regleEnCours; }

	public ChangeurRegle(){
			setRegleEnCours(Regle.values()[0]);
	}

	/**
	 * Change l'actualiseur en fonction des règles
	 * @param plateau Plateau actuel du jeu
	 * @return Un ActualiseurCellule avec le bon arbitre
	 * @see model.arbitre.Arbitre
	 * @see ActualiseurCellule
	 */
	public ActualiseurCellule changerRegle(Plateau plateau) {
		switch (getRegleEnCours()){
			case REGLE_KILLER -> {
				return new ActualiseurEtatCellule(new ArbitreKiller(plateau));
			}
			default -> {
				return new ActualiseurEtatCellule(new ArbitreConwayStyle(plateau));
			}
		}
	}
}
