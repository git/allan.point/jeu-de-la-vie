package model.plateau;

/**
 * Abstraction du clonnage de plateau
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface PrototypePlateau {
	Plateau cloner();
}
