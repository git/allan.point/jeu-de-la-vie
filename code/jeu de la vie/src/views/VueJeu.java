package views;

import javafx.beans.property.Property;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import model.Manager;
import model.Regle;
import model.actualiseur.ActualiseurTourUnParUn;
import model.cellule.Cellule;

import java.util.Random;

/**
 * Code Behind de la vue
 * @author Yohann Breuil
 * @author Allan Point
 */
public class VueJeu {

    @FXML
    private GridPane map;

    @FXML
    private ColorPicker color;

    @FXML
    private Spinner<Integer> rowGame;

    @FXML
    private Spinner<Integer> colGame;

    @FXML
    private Label nbRowGame;

    @FXML
    private Label nbColGame;

    @FXML
    private Label numTour;

    @FXML
    private ChoiceBox<Regle> regleChoiceBox;

    private Manager manager;


    /**
     * Remplie une grille fxml avec des réctangles bindés au cellules
     */
    public void createGrid() {
        map.getChildren().clear();
        for(int i=0; i < rowGame.getValue().intValue(); ++i) {
            for(int j=0; j < colGame.getValue().intValue(); ++j) {
                Rectangle rect = new Rectangle(15, 15, color.getValue());
                Cellule c = manager.getActualiseurCellule().getArbitre().getPlateau().getCell(j, i);
                rect.fillProperty().bindBidirectional(c.activeColorProperty());
                rect.setOnMouseClicked((src)->manager.inverserEtatCellule(c));
                map.add(rect, j, i);
            }
        }
    }

    /**
     * Fait naître des cellules aléatoirement sur la grille
     */
    public void generateraRandom() {
        resetGrid();
        int ligne = manager.getActualiseurCellule().getArbitre().getPlateau().getLigne();
        int colone = manager.getActualiseurCellule().getArbitre().getPlateau().getColone();
        for(int i=0; i<(colone+ligne)/2; ++i) {
            Random random = new Random();
            manager.getActualiseurCellule().getArbitre().getPlateau().getCell(random.nextInt(colone), random.nextInt(ligne)).setAlive(true);
        }
    }

    /**
     * Initalisation de la vue
     */
    public void initialize() {
        // Remplissage des valeurs de la choice box des regles
        regleChoiceBox.setItems(FXCollections.observableArrayList(Regle.values()));


        manager = new Manager();

        // Binding bidirectionel entre les regles de la vue et celles du model
        regleChoiceBox.valueProperty().bindBidirectional(manager.getChangeurRegle().regleEnCoursProperty());

        // Binding bidirectionel entre la ligne de la vue et celle du model
        rowGame.getValueFactory().valueProperty().bindBidirectional((Property) manager.getActualiseurCellule().getArbitre().getPlateau().ligneProperty());

        //Binding bidirectionel entre la colone du model et celle de la vue
        colGame.getValueFactory().valueProperty().bindBidirectional((Property) manager.getActualiseurCellule().getArbitre().getPlateau().coloneProperty());

        // Définition d'action lors de redimentionement
        manager.getActualiseurCellule().getArbitre().getPlateau().coloneProperty().addListener((src)->resetGrid());
        manager.getActualiseurCellule().getArbitre().getPlateau().ligneProperty().addListener((src)->resetGrid());

        // Initialisation des valeurs des legnes et colones
        colGame.getValueFactory().setValue(10);
        rowGame.getValueFactory().setValue(10);

        // Binding entre le colorPicker de la vue et la couleur de la cellule vivante
        color.valueProperty().bindBidirectional(Cellule.livingColorProperty());


        // Binding unidirectionel des informations à afficher (dimension de la grille et numéro de la génération
        numTour.textProperty().bind(((ActualiseurTourUnParUn)manager.getActualiseurTour()).cptTourProperty().asString());
        nbColGame.textProperty().bind(manager.getActualiseurCellule().getArbitre().getPlateau().coloneProperty().asString());
        nbRowGame.textProperty().bind(manager.getActualiseurCellule().getArbitre().getPlateau().ligneProperty().asString());
    }

    /**
     * Lancer le jeu
     * @param actionEvent
     */
    public void startGame(ActionEvent actionEvent) {
        manager.lancerJeu();
    }

    /**
     * Metre en pause le jeu
     * @param actionEvent
     */
    public void pauseGame(ActionEvent actionEvent) {
        manager.pauseJeu();
    }

    /**
     * Réinitialiser le jeu
     * @param actionEvent
     */
    public void resetGame(ActionEvent actionEvent){
        manager.stoperJeu();
        resetGrid();
    }

    /**
     * Réinitialiser la grille
     */
    public void resetGrid(){
        manager.getActualiseurCellule().getArbitre().getPlateau().resetGrille();
        createGrid();
    }
}
