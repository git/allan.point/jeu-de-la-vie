package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe qui s'ocupe de lancer l'application
 * @author Yohan Breuil
 * @author Allan Point
 */
public class Launcher extends Application {

    /**
     * Lance l'application
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(this.getClass().getResource("/fxml/VueJeu.fxml"));
        Scene sc = new Scene(root);
        primaryStage.setTitle("Jeu de la vie");
        primaryStage.setScene(sc);
        primaryStage.show();

    }
}
